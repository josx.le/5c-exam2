import React from "react";
import { Text, Image, StyleSheet, View, ScrollView, TouchableOpacity, TextInput, Button, Alert } from "react-native";
import { BlurView } from "expo-blur";

import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";
import { initializeApp } from "firebase/app";
import firebaseConfig from "./firebase-config";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from "@react-navigation/native";

const uri = "https://get.wallhere.com/photo/sky-gradient-sky-gradient-clear-sky-1422181.jpg";
const profilePicture = "https://www.trendtic.cl/wp-content/uploads/2018/05/003-Rub%C3%A9n-Belluomo-INFOR-2018.jpg";

function HomeScreen() {
  return(
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} >
      <Text>Bienvenido a Devs</Text>
    </View>
  );
}

function LoginScreen() {

  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')
  const navigation = useNavigation();

  const app = initializeApp(firebaseConfig);
  const auth = getAuth(app);

  const handleCreateAccount = () => {
    createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      console.log('Cuenta creada')
      const user = userCredential.user;
      console.log(user)
    })
    .catch(error => {
      console.log(error)
      Alert.alert(error.message)
    })
  }

  const handleSignIn = () => {
    signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      console.log('Logeado')
      const user = userCredential.user;
      console.log(user)
    })
    .catch(error => {
      console.log(error)
    })
  }

  return(
    <View style={styles.container}>
    <Image source={{ uri }} style={[styles.image, StyleSheet.absoluteFill]} />
    <View  style={{ width: 100, height: 100, backgroundColor: 'purple', position: 'absolute' }}></View>
    <ScrollView contentContainerStyle={{
      flex: 1,
      width: '100%',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
      <BlurView intensity={90}>
        <View style={styles.login}>
          <Image source={{ uri: profilePicture }} style={styles.profilePicture} />
            <View>
              <Text style={{ fontSize: 17, fontWeight: '400', color: 'white' }} >Correo</Text>
              <TextInput onChangeText={(text) => setEmail(text)} style={styles.input} placeholder="Devs11@gmail.com" />
            </View>
            <View>
              <Text style={{ fontSize: 17, fontWeight: '400', color: 'white' }} >Contraseña</Text>
              <TextInput onChangeText={(text) => setPassword(text)} style={styles.input} placeholder="Tu contraseña" secureTextEntry = {true} />
            </View>
            <TouchableOpacity onPress={handleSignIn} style={styles.button}>
              <Text  style={{fontSize: 17, fontWeight: '400', color: 'white'}}>Iniciar sesión</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleCreateAccount} style={styles.button}>
              <Text  style={{fontSize: 17, fontWeight: '400', color: 'white'}}>Crear cuenta</Text>
            </TouchableOpacity>
        </View>
      </BlurView>
    </ScrollView>
  </View>
  );
}

const Stack = createStackNavigator();

export default function App() {
  return (
   <NavigationContainer>
    <Stack.Navigator initialRouteName = "Login">
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Home" component={HomeScreen} />
    </Stack.Navigator>
   </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  login: {
    width: 350,
    height: 500,
    borderColor: '#fff',
    borderWidth: 2,
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
  },
  profilePicture: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderColor: '#fff',
    borderWidth: 1,
    marginVertical: 10,
  },
  input: {
    width: 250,
    height: 40,
    borderColor: '#fff',
    borderWidth: 2,
    borderRadius: 10,
    padding: 10,
    marginVertical: 10,
    backgroundColor: '#ffffff90',
    marginBottom: 20,
  },
  button: {
    width: 250,
    height: 40,
    borderRadius: 10,
    backgroundColor: '#00CFEB90',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    borderColor: '#fff',
    borderWidth: 1,
  }
});
